# 신규 키보드 레이아웃 추가하기(Adding new keyboard layouts) #


743/5000
_ ** 경고 : ** 이전 [문제 # 13] (https://github.com/klausw/hackerskeyboard/issues/13)에서는 현재 키보드 레이아웃 파일을 새 시스템으로 교체하려는 계획에 대해 설명합니다. 이 시점에서 나는 이런 변화가 일어나지 않을 것이라고 생각하며, 만일 그렇게 한다면 새로운 시스템으로의 자동 마이그레이션 경로가 필요할 것이다. 따라서 현재로서는 XML 파일이 여전히 레이아웃을 추가하거나 수정하는 공식적인 방법입니다 ._

개발자가 직접 읽거나 쓸 수 없는 언어의 키보드 레이아웃을 만들고 테스트하기가 매우 어렵습니다. 추가 언어에 대한 지원을 개선하는 데 큰 도움이 될 것입니다.

아직 지원되지 않는 언어에 대한 키보드 레이아웃을 제공 할 수 있는지 알려주십시오. 중복 노력을 피하기 위해 작업을 시작하기 전에 저에게 연락하십시오.



## 한계 ##

   * "shift"를 "caps lock"과 동일하게 취급합니다 (일부 레이아웃은이 두 모드에 대해 서로 다른 매핑이 필요합니다).
   * 대체 키에 직접 액세스하기 위해 AltGr 키를 직접 지원하지 않으며, 대신 해당 키를 길게 눌러 추가 문자를 제공하여 대체 가능한 팝업 미니 키보드를 표시합니다. 또는 키보드는 악센트 부호가있는 문자를 생성하는 데 사용할 수있는 데드 키를 지원합니다.
   * 중국어와 같이 키보드에 합리적으로 적용 할 수 있는 것보다 더 많은 글리프가 필요한 언어는 지원하지 않습니다. 복잡한 입력 방법을 추가 할 계획은 없습니다.이 프로젝트의 범위를 벗어납니다.
   * 단, 한국어는 지원한다.

## 개요 ##

새로운 레이아웃을 완전히 지원하는 데 필요한 세 부분이 있습니다.
  * 세로 모드의 전화기에서 사용되는 4 열 레이아웃
  * 길게 누르기 팝업 미니 키보드 내용을 채우는 데 사용되는 "대체"정의
  * 태블릿 또는 가로 모드에서 사용되는 5 열 레이아웃

4 행 키보드에는 각각 키 구성 및 문자를 정의하는`kbd_qwerty.xml '파일이 있습니다 (예 : [kbd \ _qwerty.xml] (https://github.com/klausw/hackerskeyboard/blob/master/java/res). 독일어 QWERTZ 레이아웃의 경우 /xml-de/kbd_qwerty.xml) 언어에 현재 4 행 레이아웃이없는 경우이를 조정하고 추가해야합니다.

길게 누르기 팝업으로 표시되는 문자 별 대체 문자는 언어 별`donottranslate-altchars.xml` 파일을 기반으로합니다. 여기에는 4 행 키보드의 맨 위 행에 표시된 숫자가 포함됩니다.

5 행 키보드 레이아웃은 표준 Gingerbread 4 행 키보드와 별도로 구성됩니다.

핵심 5 행 레이아웃은 [kbd \ _full.xml] (https://github.com/klausw/hackerskeyboard/blob/master/java/res/xml/kbd_full.xml) 파일에 정의되어 있으며 키 크기를 정의합니다. 각 키에 표시된 문자는 제외하고 위치를 지정하므로 언어로 수정할 필요가 없습니다. 문자 매핑은 언어별로 donottranslate-keymap.xml 파일에 정의되어 각 키 위치에 문자를 할당합니다.

언어 별 매핑의 예 :
  * 영어 QWERTY [4 행 레이아웃] (https://github.com/klausw/hackerskeyboard/blob/master/java/res/xml/kbd_qwerty.xml), [5 행 키맵] (https : // github. com / klausw / hackerskeyboard / blob / master / java / res / values ​​/ donottranslate-keymap.xml) 및 [altchars] (https://github.com/klausw/hackerskeyboard/blob/master/java/res/values/donottranslate -altchars.xml)
  * 독일어 QWERTZ [4 열 레이아웃] (https://github.com/klausw/hackerskeyboard/blob/master/java/res/xml-de/kbd_qwerty.xml), [keymap] (https://github.com /klausw/hackerskeyboard/blob/master/java/res/values-de/donottranslate-keymap.xml) 및 [altchars] (https://github.com/klausw/hackerskeyboard/blob/master/java/res/values- de / donottranslate-altchars.xml)
  * 러시아어 [4 행 레이아웃] (https://github.com/klausw/hackerskeyboard/blob/master/java/res/xml-ru/kbd_qwerty.xml), [keymap] (https://github.com/ klausw / hackerskeyboard / blob / master / java / res / values-ru / donottranslate-keymap.xml)

## keymap.xml 파일 편집 ##

5 행 키보드 레이아웃을 만들려면 각 키 위치에 기본, 이동 및 대체 문자를 할당하십시오. 대체 문자를 파일에서 직접 정의하거나 altchars 파일을 통해 간접적으로 (라틴 알파벳 언어에 권장) 정의 할 수 있습니다.

키 정의의 예 :
```
    <string name="key_ae04_main">4</string>
    <string name="key_ae04_shift">$</string>
    <string name="key_ae04_alt">£€¥</string>
```

결과는 다음과 같습니다.
```
    Main map:  "4", alternates "$£#¥"
    Shift map: "$", alternates "4£#¥"
```

키 "ae04"는 일반적인 PC 레이아웃의 5 번째 행 ( "e")에서 왼쪽부터 4 번째 키입니다.

다음은 간접적으로 지정된 대체 문자가있는 예입니다.

```
    <string name="key_ad03_main">e</string>
    <string name="key_ad03_shift">E</string>
    <string name="key_ad03_alt">@string/alternates_for_e</string>
```

`alternates_for_e` 문자열 참조는`donottranslate-altchars.xml`에서 이 항목을 사용합니다 :
```
    <string name="alternates_for_e">3éèêëē€</string>
```

첫 번째 대체 문자는 설정에서 구성된 경우 키에 대한 힌트로 그려집니다. 나머지 대체 문자 (있는 경우)를 가장 빈번한 순서에서 가장 빈번한 순서로 나열하십시오. 키보드의 오른쪽 절반에있는 키는 오른쪽에서 왼쪽으로 반전되어 그려집니다. (이것은 원래 진저 브레드 AOSP 동작과 다릅니다.)

현재 레이아웃 정의 파일의 대부분은 기본 및 시프트 문자를 처음 두 개의 대체 문자로 반복합니다. 이제는 구성 옵션을 기반으로 자동으로 수행하므로 현재 버전에서는 더 이상 필요하지 않습니다. 별도의 숫자 행이있는 5 행 레이아웃의 대체에서 숫자를 제거하는 등 레이아웃을 초기화 할 때 중복 문자가 자동으로 제거됩니다.

문자의 경우`@ string / alternates_for_X`를 사용하여`values ​​/ donottranslate-altchars.xml`에서 사전 정의 된 목록을 참조 할 수 있습니다. 현지화 된 ~~`values-ZZ / donottranslate-altchars.xml` ~~ 파일에 추가 항목을 추가하지 마십시오 **. 주 파일의 기존 값을 재정의하는 것만 합법적입니다. 현재 값이없는 경우 각 키에 대해 대체 항목을 직접 추가하십시오.

다음과 같이 백 슬래시 이스케이프 및 XML 엔터티를 사용하십시오.
```
     &  ->  &amp;
     <  ->  &lt;
     >  ->  &gt;
     \  ->  \\
     @  ->  \@
     ?  ->  \?
     '  ->  \'
     "  ->  \"
```

## 데드 키 및 발음 구별 부호 (악센트) ##

유니 코드 "분음 부호 표시"를 사용하여 악센트 또는 다른 분음 부호를위한 데드 키를 추가 할 수 있습니다. 예를 들면 다음과 같습니다.


```
    <string name="key_ac10_main">&#x301;</string>
    <string name="key_ac10_shift">&#x308;</string>
    <string name="key_ac10_alt">&#x313;</string>
```

목록은 https://unicode.org/charts/PDF/U0300.pdf를 참조하십시오. 시스템의 글꼴 및 글리프 구성 엔진이 원하는 모든 조합을 지원하지 않을 수 있으며이 조합으로 인해 강조 문자 대신 사각형 자리 표시자가 생길 수 있습니다.

y 기본적으로, 데드 키는 ** 다음 ** 문자를 수정합니다. 적절한 동작이 ** 이전 ** 문자를 대신 수정하는 것이라면 코드에서 예외로 나열해야합니다 (예 : 태국어의 개정 06a7a2378fd9 참조).