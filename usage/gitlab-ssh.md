2. 프로젝트 만들고 소스 올리기 (실제 소스 업로드 및 master 관리 안할 분은 skip!)

- gitlab 로그인 > project > new project 생성

- local PC에서 소스 업로드 방법 (아래 방법은 MAC 기준입니다)

* ssh 생성

- 터미널을 열고 ssh-keygen 입력

- ssh 생성 원하는 경로 입력 (입력안하는 기본값을 생성)

- 비번입력, 비번 재입력



* ssh 등록하기

위에서 생성했던 ssh를 cat ~/.ssh/id_rsa.pub 를 통해서 연다.

그리고 해당 내용 모두 복사 (ssh-rsa 부터 끝까지...)

그리고 gitlab 사이트로 이동 계정의 setting > SSH Keys 메뉴로 이동

위의 칸에 등록

​

* 위에서 만든 프로젝트에 소스 업로드 하기

터미널에 아래 내용 입력 후 실행

- 프로젝트 셋업

git config --global user.name "회원가입 시 입력한 이름" 

git config --global user.email "회원가입 이메일"

- 셋업 후 본인 PC에 프로젝트 폴더 생성

cd 본인의 pc의 만들고자 하는 경로까지 이동

git clone https://gitlab.com/본인의git주소

- 프로젝트 폴더에 업로드 할 파일&폴더 복사 후 업로드

git add --all

git commit -am "커밋 코멘트"

git push

gitlab 사이트 접속해서 확인


3. gitlab 사이트에서 branch 추가 생성

위에서 처럼 업로드까지 마치면 master라는 기본 branch가 생성된 것이다.

하지만 협업을 통해서 진행하려면 기본 master 외에 별도의 dev용  branch를 한개 더 생성하여 관리한다

이후 dev에 commit하고 push 한 후 모두 확인한 다음에 master와 merge하여 deploy하는 방식이다.

​
=====================================
  
  138  cd 패키지명

  139  git init

  153  git status



  195  git remote add 패키지명 https://gitlab.com/계정/패키지명.git

  touch mach.md 

    git add .

   git commit -am "initial commit"

  198  git push --set-upstream 패키지명 master

 git remote add origin https://gitlab.com/amoebamach/패키지명.git

--------------- 이후 테스트 -------
  199  cat > test01.md

  200  git add .
  201  git commit -am "테스트"
  202  git push
  203  history

 git remote add origin https://gitlab.com/amoebamach/패키지명.git

